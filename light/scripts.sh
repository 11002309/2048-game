#!/bin/bash
#Install
sudo apt -y update
sudo apt -y install nginx-full
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx_latest.conf
sudo cp /home/mat/2048-game/light/nginx.conf /etc/nginx/nginx.conf
sudo cp /home/mat/2048-game/light/2048-game.service /etc/systemd/system/
cd
curl -s https://deb.nodesource.com/setup_16.x | sudo bash
sudo apt install nodejs -y

cd 2048-game
npm install --include=dev
npm run build


#Service
sudo systemctl restart nginx
sudo systemctl enable nginx --now
sudo systemctl enable 2048-game --now

